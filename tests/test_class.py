# -*- coding: UTF-8 -*-

class TestClass:
    def test_one(self):
        # pass
        x = "this"
        assert "h" in x

    def test_two(self):
        # failed
        x = "hello"
        assert hasattr(x, "check")
