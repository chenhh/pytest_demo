# -*- coding: UTF-8 -*-

import pytest

@pytest.fixture
def smtp_connection():
    import smtplib

    return smtplib.SMTP("smtp.gmail.com", 587, timeout=5)
#

def test_ehlo(smtp_connection):
    # 此處需要smtp_connection的fixture
    response, msg = smtp_connection.ehlo()
    assert response == 250
    # for demo purposes
    assert 0


def test_noop(smtp_connection):
    response, msg = smtp_connection.noop()
    assert response == 250
    # for demo purposes
    # assert 0