# -*- coding: UTF-8 -*-
from my_module import MyModule


def test_get_true():
    assert MyModule.get_true() is True


def test_get_false():
    assert MyModule.get_false() is False


def test_inst_get_true():
    mod = MyModule()
    assert mod.inst_get_true() is True


def test_inst_get_false():
    mod = MyModule()
    assert mod.inst_get_false() is False
