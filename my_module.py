# -*- coding: UTF-8 -*-

class MyModule:
    @staticmethod
    def get_true():
        return True

    @staticmethod
    def get_false():
        return False

    def inst_get_true(self):
        return True

    def inst_get_false(self):
        return False
